###############################################################################
# File         : ori.S
# Project      : EE180 Lab 2: MIPS
#
# Standards/Formatting:
#   MIPS gas, soft tab, 80 column
#
# Description:
#   Test the functionality of the 'ori' instruction.
#
###############################################################################

    .section .boot, "x"
    .balign 4
    .set    noreorder
    .global boot
    .ent    boot
boot:
    lui     $s0, 0x8002         # Load the base address of the status/test registers
    ori     $s1, $0, 1          # Prepare the 'done' status
    nop
    nop

    lui     $t1, 0xffff
    nop
    nop
    ori     $t1, $0, 0xffff
    nop
    nop
    lui     $t2, 1		#t2 = 0x00010000
    nop
    nop
    sub     $t1, $t2, $t1       #t1 should = 1
    nop
    nop
    slti    $t1, $t1, 2		
    nop
    nop
    sw      $t1, 4($s0)         # Set the test result
    sw      $s1, 0($s0)         # Set 'done'

$done:
    j       $done
    nop

    .end boot
